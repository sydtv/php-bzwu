import axios from 'axios';

// Get all category
export const getAllCategoriesReq = () => axios.get('http://localhost/php-ict/abschlussprojekt_einkaufsliste/data/api/category/read.php');

// Get single category
export const getSingleCategoryReq = (id) => axios.get('http://localhost/php-ict/abschlussprojekt_einkaufsliste/data/api/category/read_single.php?id=' + id);

// Create category
export const createCategoryReq = (category) => axios.post('http://localhost/php-ict/abschlussprojekt_einkaufsliste/data/api/category/create.php', category);

// Udpate category
export const updateCategoryReq = (category) => axios.put('http://localhost/php-ict/abschlussprojekt_einkaufsliste/data/api/category/update.php', category);

// Delete category
export const deleteCategoryReq = (id) => axios.delete('http://localhost/php-ict/abschlussprojekt_einkaufsliste/data/api/category/delete.php?id=' + id);