import axios from 'axios';

// Get all items
export const getAllItemsReq = () => axios.get('http://localhost/php-ict/abschlussprojekt_einkaufsliste/data/api/item/read.php');

// Get single item
export const getSingleItemReq = (id) => axios.get('http://localhost/php-ict/abschlussprojekt_einkaufsliste/data/api/item/read_single.php?id=' + id);

// Create item
export const createItemReq = (item) => axios.post('http://localhost/php-ict/abschlussprojekt_einkaufsliste/data/api/item/create.php', item);

// Udpate item
export const updateItemReq = (item) => axios.put('http://localhost/php-ict/abschlussprojekt_einkaufsliste/data/api/item/update.php', item);

// Delete item
export const deleteItemReq = (id) => axios.delete('http://localhost/php-ict/abschlussprojekt_einkaufsliste/data/api/item/delete.php?id=' + id);
