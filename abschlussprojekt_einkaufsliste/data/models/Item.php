<?php

class Item
{
    // DB-Properties
    private $conn;
    private $table = 'items';

    // Item-Properties
    public $id;
    public $category_id;
    public $category_name;
    public $name;
    public $amount;
    public $bought;
    public $created_at;

    // Constructor
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // Get Items
    public function read()
    {
        // SQL-Query
        $query = 'SELECT c.name as category_name, 
                    i.id,
                    i.category_id,
                    i.name,
                    i.amount,
                    i.bought,
                    i.created_at
                  FROM 
                    ' . $this->table . ' i 
                  LEFT JOIN
                    categories c ON i.category_id = c.id
                  ORDER BY 
                    i.created_at DESC';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Execute query
        $stmt->execute();

        return $stmt;
    }

    // Get Single Item
    public function read_single()
    {;
        // SQL-Query
        $query = 'SELECT c.name as category_name, 
                    i.id,
                    i.category_id,
                    i.name,
                    i.amount,
                    i.bought,
                    i.created_at
                  FROM 
                    ' . $this->table . ' i 
                  LEFT JOIN
                    categories c ON i.category_id = c.id
                  WHERE 
                    i.id = ?
                  LIMIT 0,1';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Execute query
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set properties
        $this->name = $row['name'];
        $this->amount = $row['amount'];
        $this->bought = $row['bought'];
        $this->created_at = $row['created_at'];
        $this->category_id = $row['category_id'];
        $this->category_name = $row['category_name'];
    }

    // Create item
    public function create() {
        // Create query
        $query = 'INSERT INTO ' . $this->table . ' 
                  SET
                    name = :name,
                    amount = :amount,
                    category_id = :category_id';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->amount = htmlspecialchars(strip_tags($this->amount));
        $this->category_id = htmlspecialchars(strip_tags($this->category_id));

        // Bind data
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':amount', $this->amount);
        $stmt->bindParam(':category_id', $this->category_id);

        // Execute query
        if($stmt->execute()) {
            http_response_code(200);
            return true;
        }

        http_response_code(500);
        printf('Error: %s \n', $stmt->error);

        return false;
    }

    // Update item
    public function update() {
        // Create query
        $query = 'UPDATE ' . $this->table . ' 
                  SET
                    name = :name,
                    amount = :amount,
                    category_id = :category_id,
                    bought = :bought
                  WHERE
                    id = :id;';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->id = htmlspecialchars(strip_tags($this->id));
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->amount = htmlspecialchars(strip_tags($this->amount));
        $this->bought = htmlspecialchars(strip_tags($this->bought));
        $this->category_id = htmlspecialchars(strip_tags($this->category_id));

        // Bind data
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':amount', $this->amount);
        $stmt->bindParam(':bought', $this->bought);
        $stmt->bindParam(':category_id', $this->category_id);

        // Execute query
        if($stmt->execute()) {
            http_response_code(200);
            return true;
        }

        http_response_code(500);
        printf('Error: %s \n', $stmt->error);

        return false;
    }

    // Delete car
    public function delete() {
        // Create query
        $query = 'DELETE FROM ' . $this->table . ' 
                  WHERE
                    id = ?';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->id = htmlspecialchars(strip_tags((int)$this->id));

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Execute query
        if($stmt->execute()) {
            http_response_code(200);
            return true;
        }

        http_response_code(500);
        printf('Error: %s \n', $stmt->error);

        return false;
    }
}