<?php

class Category
{
    // DB-Properties
    private $conn;
    private $table = 'categories';

    // Fuel-Properties
    public $id;
    public $name;

    // Constructor
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // Get categories
    public function read()
    {
        // SQL-Query
        $query = 'SELECT 
                    id, 
                    name
                  FROM 
                    ' . $this->table . ' 
                  ORDER BY 
                    id DESC';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Execute query
        $stmt->execute();

        return $stmt;
    }

    public function read_single(){
        // Create query
        $query = 'SELECT
          id,
          name
        FROM
          ' . $this->table . '
        WHERE id = ?
        LIMIT 0,1';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Execute query
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set properties
        $this->id = $row['id'];
        $this->name = $row['name'];
    }

    // Create category
    public function create() {
        // Create query
        $query = 'INSERT INTO ' . $this->table . ' 
                  SET
                    name = :name';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->name = htmlspecialchars(strip_tags($this->name));

        // Bind data
        $stmt->bindParam(':name', $this->name);

        // Execute query
        if($stmt->execute()) {
            http_response_code(200);
            return true;
        }

        http_response_code(500);
        printf('Error: %s \n', $stmt->error);

        return false;
    }

    // Update category
    public function update() {
        // Create query
        $query = 'UPDATE ' . $this->table . ' 
                  SET
                    name = :name
                  WHERE
                    id = :id';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->id = htmlspecialchars(strip_tags($this->id));
        $this->name = htmlspecialchars(strip_tags($this->name));

        // Bind data
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':name', $this->name);

        // Execute query
        if($stmt->execute()) {
            return true;
        }

        printf('Error: %s \n', $stmt->error);

        return false;
    }

    // Delete category
    public function delete() {
        // Create query
        $query = 'DELETE FROM ' . $this->table . ' 
                  WHERE
                    id = ?';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->id = htmlspecialchars(strip_tags((int)$this->id));

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Execute query
        if($stmt->execute()) {
            http_response_code(200);
            return true;
        }

        http_response_code(500);
        printf('Error: %s \n', $stmt->error);

        return false;
    }
}