<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: DELETE");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Methods,Content-Type,X-Requested-With");

include_once './../../config/Database.php';
include_once './../../models/Category.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate item object
$category = new Category($db);

// Get id to delete
$category->id = isset($_GET['id']) ? $_GET['id'] : die();

// Delete item
if($category->delete()) {
    echo json_encode(
        array('message' => 'Category deleted')
    );
} else {
    echo json_encode(
        array('message' => 'Category not deleted')
    );
}