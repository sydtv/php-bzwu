<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Methods,Content-Type,X-Requested-With");

include_once './../../config/Database.php';
include_once './../../models/Category.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate car object
$category = new Category($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));

if(empty($data)) {
    return false;
}

$category->name = $data->name;

// Create item
if($category->create()) {
    echo json_encode(
        array('message' => 'Category created')
    );
} else {
    echo json_encode(
        array('message' => 'Category not created')
    );
}