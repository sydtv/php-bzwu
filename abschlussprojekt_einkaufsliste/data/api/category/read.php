<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/Category.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate category object
$category = new Category($db);

// Auto-Query
$result = $category->read();

// Row Count
$num = $result->rowCount();

// Check if categories exist
if ($num > 0) {

    // Auto-Array
    $categories_arr = array();
    $categories_arr['data'] = array();

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $category_item = array(
            'id' => $id,
            'name' => $name
        );

        // Push to data-array
        array_push($categories_arr['data'], $category_item);
    }

    // Make JSON
    echo json_encode($categories_arr);

} else {
    // Output if no categories exist
    echo json_encode(
        array('message' => 'No categories found')
    );
}