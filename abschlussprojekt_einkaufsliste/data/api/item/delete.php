<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: DELETE");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Methods,Content-Type,X-Requested-With");

include_once './../../config/Database.php';
include_once './../../models/Item.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate item object
$item = new Item($db);

// Get id to delete
$item->id = isset($_GET['id']) ? $_GET['id'] : die();

// Delete item
if($item->delete()) {
    echo json_encode(
        array('message' => 'Item deleted')
    );
} else {
    echo json_encode(
        array('message' => 'Item not deleted')
    );
}