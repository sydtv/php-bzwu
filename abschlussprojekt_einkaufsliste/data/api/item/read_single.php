<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/Item.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate item object
$item = new Item($db);

// Get ID
$item->id = isset($_GET['id']) ? $_GET['id'] : die();

// Get item
$item->read_single();

// Item-Array
$item_arr = array(
    'id' => $item->id,
    'name' => $item->name,
    'amount' => $item->amount,
    'bought' => $item->bought,
    'created_at' => $item->created_at,
    'category_id' => $item->category_id,
    'category_name' => $item->category_name
);

// Make JSON
print_r(json_encode($item_arr));