<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/Item.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate item object
$car = new Item($db);

// Item-Query
$result = $car->read();

// Row Count
$num = $result->rowCount();

// Check if items exist
if ($num > 0) {

    // Item-Array
    $items_arr = array();
    $items_arr['data'] = array();

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $item_item = array(
            'id' => $id,
            'name' => $name,
            'amount' => $amount,
            'bought' => $bought,
            'created_at' => $created_at,
            'category_id' => $category_id,
            'category_name' => $category_name
        );

        // Push to data-array
        array_push($items_arr['data'], $item_item);
    }

    // Make JSON
    echo json_encode($items_arr);

} else {
    // Output if no items exist
    echo json_encode(
        array('message' => 'No items found')
    );
}