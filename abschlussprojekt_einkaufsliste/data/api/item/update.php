<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Methods,Content-Type,X-Requested-With");

include_once './../../config/Database.php';
include_once './../../models/Item.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate item object
$item = new Item($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));

// Set id to update
$item->id = $data->id;

$item->name = $data->name;
$item->amount = $data->amount;
$item->bought = $data->bought;
$item->category_id = $data->category_id;

// Update item
if($item->update()) {
    echo json_encode(
        array('message' => 'Item updated')
    );
} else {
    echo json_encode(
        array('message' => 'Item not updated')
    );
}