<?php

class Database
{
    // DB-Eigenschaften
    private $host = 'localhost';
    private $db_name = 'sql_shoppinglist';
    private $username = 'root';
    private $password = '';
    private $conn;

    // DB-Verbindung
    public function connect()
    {
        $this->conn = null;

        try {
            $this->conn = new PDO(
                'mysql:host=' . $this->host . ';dbname=' . $this->db_name,
                $this->username,
                $this->password
            );

            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Connection Error: ' . $e->getMessage();
        }

        return $this->conn;
    }
}