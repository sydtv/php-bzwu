DROP DATABASE IF EXISTS `sql_shoppinglist`;
CREATE DATABASE `sql_shoppinglist`;
USE `sql_shoppinglist`;

SET NAMES utf8mb4;
SET character_set_client = utf8mb4;

CREATE TABLE `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE `items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_id` int NOT NULL,
  `name` varchar(64) NOT NULL,
  `amount` varchar(64) NOT NULL,
  `bought` int NOT NULL default(0),
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT FK_CategoryID FOREIGN KEY (category_id) REFERENCES categories(id) on delete cascade on update cascade
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;