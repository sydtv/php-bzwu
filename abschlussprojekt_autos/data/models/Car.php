<?php

class Car
{
    // DB-Properties
    private $conn;
    private $table = 'cars';

    // Car-Properties
    public $id;
    public $fuel_id;
    public $fuel_name;
    public $name;
    public $color;
    public $created_at;

    // Constructor
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // Get Cars
    public function read()
    {
        // SQL-Query
        $query = 'SELECT f.name as fuel_name, 
                    c.id,
                    c.fuel_id,
                    c.name,
                    c.color,
                    c.created_at
                  FROM 
                    ' . $this->table . ' c 
                  LEFT JOIN
                    fuels f ON c.fuel_id = f.id
                  ORDER BY 
                    c.created_at DESC';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Execute query
        $stmt->execute();

        return $stmt;
    }

    // Get Single Car
    public function read_single()
    {
        // SQL-Query
        $query = 'SELECT f.name as fuel_name, 
                    c.id,
                    c.fuel_id,
                    c.name,
                    c.color,
                    c.created_at
                  FROM 
                    ' . $this->table . ' c 
                  LEFT JOIN
                    fuels f ON c.fuel_id = f.id
                  WHERE 
                    c.id = ?
                  LIMIT 0,1';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Execute query
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set properties
        $this->name = $row['name'];
        $this->color = $row['color'];
        $this->created_at = $row['created_at'];
        $this->fuel_id = $row['fuel_id'];
        $this->fuel_name = $row['fuel_name'];
    }

    // Create car
    public function create() {
        // Create query
        $query = 'INSERT INTO ' . $this->table . ' 
                  SET
                    name = :name,
                    color = :color,
                    fuel_id = :fuel_id';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->color = htmlspecialchars(strip_tags($this->color));
        $this->fuel_id = htmlspecialchars(strip_tags($this->fuel_id));

        // Bind data
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':color', $this->color);
        $stmt->bindParam(':fuel_id', $this->fuel_id);

        // Execute query
        if($stmt->execute()) {
            return true;
        }

        printf('Error: %s \n', $stmt->error);

        return false;
    }

    // Update car
    public function update() {
        // Create query
        $query = 'UPDATE ' . $this->table . ' 
                  SET
                    name = :name,
                    color = :color,
                    fuel_id = :fuel_id
                  WHERE
                    id = :id;';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->id = htmlspecialchars(strip_tags($this->id));
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->color = htmlspecialchars(strip_tags($this->color));
        $this->fuel_id = htmlspecialchars(strip_tags($this->fuel_id));

        // Bind data
        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':color', $this->color);
        $stmt->bindParam(':fuel_id', $this->fuel_id);

        // Execute query
        if($stmt->execute()) {
            return true;
        }

        printf('Error: %s \n', $stmt->error);

        return false;
    }

    // Delete car
    public function delete() {
        // Create query
        $query = 'DELETE FROM ' . $this->table . ' 
                  WHERE
                    id = :id';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->id = htmlspecialchars(strip_tags($this->id));

        // Bind data
        $stmt->bindParam(':id', $this->id);

        // Execute query
        if($stmt->execute()) {
            return true;
        }

        printf('Error: %s \n', $stmt->error);

        return false;
    }
}