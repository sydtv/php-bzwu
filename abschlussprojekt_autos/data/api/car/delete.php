<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: DELETE");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Methods,Content-Type,X-Requested-With");

include_once './../../config/Database.php';
include_once './../../models/Car.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate car object
$car = new Car($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));

// Set id to delete
$car->id = $data->id;

// Delete car
if($car->delete()) {
    echo json_encode(
        array('message' => 'Car deleted')
    );
} else {
    echo json_encode(
        array('message' => 'Car not deleted')
    );
}