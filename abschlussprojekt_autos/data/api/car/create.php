<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Methods,Content-Type,X-Requested-With");

include_once './../../config/Database.php';
include_once './../../models/Car.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate car object
$car = new Car($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));

$car->name = $data->name;
$car->color = $data->color;
$car->fuel_id = $data->fuel_id;

// Create car
if($car->create()) {
    echo json_encode(
        array('message' => 'Car created')
    );
} else {
    echo json_encode(
        array('message' => 'Car not created')
    );
}