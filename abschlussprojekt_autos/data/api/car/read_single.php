<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/Car.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate car object
$car = new Car($db);

// Get ID
$car->id = isset($_GET['id']) ? $_GET['id'] : die();

// Get car
$car->read_single();

// Car-Array
$car_arr = array(
    'id' => $car->id,
    'name' => $car->name,
    'color' => $car->color,
    'created_at' => $car->created_at,
    'fuel_id' => $car->fuel_id,
    'fuel_name' => $car->fuel_name
);

// Make JSON
print_r(json_encode($car_arr));