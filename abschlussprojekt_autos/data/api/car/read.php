<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/Car.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate car object
$car = new Car($db);

// Car-Query
$result = $car->read();

// Row Count
$num = $result->rowCount();

// Check if cars exist
if ($num > 0) {

    // Car-Array
    $cars_arr = array();
    $cars_arr['data'] = array();

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $car_item = array(
            'id' => $id,
            'name' => $name,
            'color' => $color,
            'created_at' => $created_at,
            'fuel_id' => $fuel_id,
            'fuel_name' => $fuel_name
        );

        // Push to data-array
        array_push($cars_arr['data'], $car_item);
    }

    // Make JSON
    echo json_encode($cars_arr);

} else {
    // Output if no cars exist
    echo json_encode(
        array('message' => 'No cars found')
    );
}