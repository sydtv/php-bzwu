<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Methods,Content-Type,X-Requested-With");

include_once './../../config/Database.php';
include_once './../../models/Fuel.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate fuel object
$fuel = new Fuel($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));

$fuel->name = $data->name;

// Create fuel
if($fuel->create()) {
    echo json_encode(
        array('message' => 'Fuel created')
    );
} else {
    echo json_encode(
        array('message' => 'Fuel not created')
    );
}