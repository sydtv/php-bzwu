<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Fuel.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate fuel object
$fuel = new Fuel($db);

// Get ID
$fuel->id = isset($_GET['id']) ? $_GET['id'] : die();

// Get fuel
$fuel->read_single();

// Create array
$fuel_arr = array(
    'id' => $fuel->id,
    'name' => $fuel->name
);

// Make JSON
print_r(json_encode($fuel_arr));