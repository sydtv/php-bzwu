<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/Fuel.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate fuel object
$fuel = new Fuel($db);

// Auto-Query
$result = $fuel->read();

// Row Count
$num = $result->rowCount();

// Check if fuels exist
if ($num > 0) {

    // Auto-Array
    $fuels_arr = array();
    $fuels_arr['data'] = array();

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $fuel_item = array(
            'id' => $id,
            'name' => $name
        );

        // Push to data-array
        array_push($fuels_arr['data'], $fuel_item);
    }

    // Make JSON
    echo json_encode($fuels_arr);

} else {
    // Output if no fuels exist
    echo json_encode(
        array('message' => 'No fuels found')
    );
}