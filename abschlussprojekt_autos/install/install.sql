DROP DATABASE IF EXISTS `sql_autogarage`;
CREATE DATABASE `sql_autogarage`; 
USE `sql_autogarage`;

SET NAMES utf8mb4;
SET character_set_client = utf8mb4;
SET GLOBAL sql_mode='';

CREATE TABLE `fuels` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

CREATE TABLE `cars` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fuel_id` int NOT NULL,
  `name` varchar(64) NOT NULL,
  `color` varchar(64) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT FK_FuelID FOREIGN KEY (fuel_id) REFERENCES fuels(id) on delete cascade on update cascade
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;